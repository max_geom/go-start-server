package main

import (
	"Server/app"
	"fmt"
	"os"
)

func main() {
	if len(os.Args) == 2 {
		app.PORT = os.Args[1]
	}
	fmt.Println("Start server on port", app.PORT)
	app.Start()
}
package middleware

import "net/http"

func HeaderMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		request.Header.Set("Content-Type", "application/json")
		next.ServeHTTP(writer, request)
	})
}

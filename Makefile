.PHONY: build

build:
	go build -v ./cmd/Server/main.go

.DEFAULT_GOAL := build
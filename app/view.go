package app

import (
	"encoding/json"
	"net/http"
)

func startPage(writer http.ResponseWriter, request *http.Request) {
	json.NewEncoder(writer).Encode("Hello world")
}

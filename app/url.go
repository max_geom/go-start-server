package app

import (
	"github.com/gorilla/mux"
)

func baseRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", startPage).Methods("GET")
	return router
}

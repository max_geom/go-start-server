package app

import (
	"Server/middleware"
	"log"
	"net/http"
)

func Start() {
	router := baseRouter()
	router.Use(middleware.HeaderMiddleware)
	log.Fatal(http.ListenAndServe(PORT, router))
}

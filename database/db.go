package database

import (
	"Server/app"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
)

func Connect() *gorm.DB {
	db, err := gorm.Open("sqlite3", app.DataBaseDir)
	if err == nil {
		log.Fatal(err)
		return nil
	}
	return db
}
